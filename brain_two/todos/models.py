from django.db import models
from django.utils import timezone


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(default=timezone.now)


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    todo_list = models.ForeignKey(
        TodoList, related_name="items", on_delete=models.CASCADE
    )
